FROM python:3.8

WORKDIR /app

RUN pip install pipenv

COPY Pipfile .
COPY Pipfile.lock .

RUN pipenv install

COPY models models
COPY device_activity.py .
COPY main.py .

CMD ["pipenv", "run", "python3", "main.py"]