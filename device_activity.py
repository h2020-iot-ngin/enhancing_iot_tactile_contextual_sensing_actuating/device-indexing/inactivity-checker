# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Inactivity Checker.
#
# Inactivity Checker is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Inactivity Checker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Inactivity Checker. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

import os
import json
import requests
from datetime import datetime
from models.update import Update

def log_devices():
    """
    Create a log of all the devices, to determine which are new (have not yet been updated), 
    in order to enable the appropriate commands.
    :param: None
    :return: response of update
    """

    # Create url to get all entities, along with the dateCreated and dateModified attributes
    url = form_url('entity')
    url = f"{url}?options=count"

    if os.getenv('USE_FIWARE_SERVICE_HEADER') == "True":
        headers = {
            "Fiware-Service": os.getenv('FIWARE_SERVICE_HEADER'),
            "Fiware-ServicePath": os.getenv('FIWARE_SERVICE_PATH_HEADER')
        }

        response = requests.get(url, headers=headers)
    else:
        response = requests.get(url)
    
    total_entities = int(response.headers['Fiware-Total-Count'])
    devices = pagination(total_entities, 'entity')

    status = check_new_device_status(devices)

    response = check_activity_status(devices, status)
    return response


def check_new_device_status(devices: list):
    """
    Check if a device has the same creation and modification time for all its attributes. 
    This device is condidered new.
    :param devices: a list of all the available devices on Orion Context Broker
    :return: a list of the devices that require a change in 'new_device' attribute
    """
    status = []

    for device in devices:
        # Check if all attributes of the entity have the same creation and modifcation time 
        # If so, then the device is considered new
        new_device = all(device[key]['metadata']['dateCreated']['value'] == device[key]['metadata']['dateModified']['value'] for key in set(device) - {'id', 'type', 'active', 'newDevice'})
        
        if 'newDevice' not in device:
            status.append({'type': device['type'], 'id': device['id'], 'newDevice': {'value': new_device}})
        elif 'newDevice' in device and new_device != device['newDevice']['value']:
            status.append({'type': device['type'], 'id': device['id'], 'newDevice': {'value': new_device}})
        else:
            continue

    print(status)
    return status


def check_activity_status(devices: list, status: list):
    """
    Check if a device is active, by determining
    if an X amount of time has passed since the last update. 
    If so, the device is inactive, otherwise it is considered active.
    :param devices: a list of all the available devices on Orion Context Broker
    :param status: a list that may contain devices that need a change in 'new_device' attribute
    :return: the response to the 'PUT' request to Orion Context Broker    
    """
    current_time = datetime.utcnow()

    for device in devices:
        if all(check_time((current_time - string_to_datetime(device[key]['metadata']['dateModified']['value'])).total_seconds()) == False for key in set(device) - {'id', 'type', 'active', 'newDevice'}):
            active = False
        else:
            active = True
        
        if 'active' not in device:
            status.append({'type': device['type'], 'id': device['id'], 'active': {'value': active}})
        elif 'active' in device and active != device['active']['value']:
            status.append({'type': device['type'], 'id': device['id'], 'active': {'value': active}})
        else:
            continue
    print(status)
    response = send_status_update(status)

    return response
        

def string_to_datetime(date_string):
    """
    Convert string datetime to datetime object
    :param date_string: the date in string format
    :return: the date in datetime format 
    """
    return datetime.strptime(date_string, '%Y-%m-%dT%H:%M:%S.%fZ')


def check_time(time_difference):
    """
    Check the difference (timedelta object) between the current time 
    and the last update time to determine if device is active or not
    :param time_difference: the difference between current time and lat update
    :return: a boolean that shows if device is active or not 
    """
    active = False
    inactivity_period = os.getenv('INACTIVITY_PERIOD_SECONDS')
    inactivity_period = float(inactivity_period)

    if time_difference < inactivity_period:
        active = True
    
    return active 


def send_status_update(status: list):
    """
    Send a list with the updates required for 'new_device' and 'active' attributes.
    It could be empty if no changes are required.
    :param: status: a list that may contain devices that need a change in 'new_device' and 'active' attributes
    :return: the response to the 'PUT' request to Orion Context Broker    
    """
    if len(status) > 0:
        actionType = "append"

        if os.getenv('USE_FIWARE_SERVICE_HEADER') == "True":
            headers = {
                    "Content-Type": "application/json",
                    "Fiware-Service": os.getenv('FIWARE_SERVICE_HEADER'),
                    "Fiware-ServicePath": os.getenv('FIWARE_SERVICE_PATH_HEADER')
            }
        else:
            headers = {
                    "Content-Type": "application/json"
            }

        url = form_url('update')

        update = Update(actionType, status)
        request = to_json(update)
        
        response = requests.post(url, data=request, headers=headers)

        return response
    else:
        response = "Everything is up-to-date!"
        return response


def form_url(event: str):
    """
    Form the URL for the requests
    :param event: form the URL for either getting entities or updating them
    :return: the appropriate URL
    """
    url = os.getenv('ORION_URL')
    if event == "entity":
        url = f"{url}/v2/entities"
    elif event == "update":
        url = f"{url}/v2/op/update"

    return url


def to_json(obj):
    """
    Turn body of request to JSON format
    :param obj: the body of the request
    :return: JSON formatted body
    """
    request = json.dumps(obj.__dict__, indent=3)
    return request


def pagination(total_entities: int, event: str):
    """
    Method to help with the retrieval of entities, 
    when they are large in number
    :param total_entities: the total number of the entities stored in Orion
    :param event: form the URL for either getting entities or updating them
    :return: a list of all the entities
    """
    counter = 0
    offset = 0
    limit = int(os.getenv('ENTITIES_LIMIT'))
    data = []

    while offset <= total_entities:
        offset = limit * counter
        url = form_url(event)
        url = f"{url}?offset={offset}&limit={limit}&metadata=dateCreated,dateModified"

        if os.getenv('USE_FIWARE_SERVICE_HEADER') == "True":
            headers = {
                "Fiware-Service": os.getenv('FIWARE_SERVICE_HEADER'),
                "Fiware-ServicePath": os.getenv('FIWARE_SERVICE_PATH_HEADER')
            }

            response = requests.get(url, headers=headers)
        else:
            response = requests.get(url)
        
        data.append(response.json())
        counter += 1
    
    data = [item for sublist in data for item in sublist]

    return data