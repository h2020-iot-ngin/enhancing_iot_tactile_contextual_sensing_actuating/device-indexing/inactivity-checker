# Inactivity Checker

## Purpose

The `Inactivity Checker` is used to determine whether a device is new (has just been created), as well as its activity status. To determine the activity status, the `Inactivity Checker` checks when was the last modification time, for all available devices on the IoT-NGIN Device Indexing (IDI) module, and if it determines to have not been updated for a certain time range, it is deemed inactive.

## Environmental Variables

In order for the project to work correctly, a set of environmental variables need to be set: 

| Environmental Variable | Role | Example Value | 
|       ---              | ---  |     ---       |
| ORION_URL | The URL Orion Context Broker is available at | `http://localhost:1026` |
| INACTIVITY_PERIOD_SECONDS | The time limit (in seconds) that the `Inactivity Checker` uses to determine the activity status of a device. If the device has not been updated for X amount of seconds, then it is considered inactive | `86400` | 
| ENTITIES_LIMIT | Set `limit` variable, required for [pagination mechanism](https://fiware-orion.readthedocs.io/en/master/user/pagination/index.html) | `10` |
| USE_FIWARE_SERVICE_HEADER | If set `False` the HTTP requests will not include the appropriate header to query the entities stored under a [Fiware-Service](#https://github.com/FIWARE/tutorials.IoT-Agent) | `True` |
| FIWARE_SERVICE_HEADER | If the `USE_FIWARE_SERVICE_HEADER` variable is set to `True`, then the HTTP requests will include the `Fiware-Service` header, determined by this variable's value | `openiot` |
| FIWARE_SERVICE_PATH_HEADER | If the `USE_FIWARE_SERVICE_HEADER` variable is set to `True`, then the HTTP requests will include the `Fiware-ServicePath` header, determined by this variable's value | `/` |

Note: In case there are multile `Fiware-Service` and `Fiware-ServicePath` headers that need to be tracked, the solution is to create a new `CronJob` for every set of headers. 

### Example

For 2 device types (e.g. `Synfield` and `Mobile`) that have different inactivity periods, we will need to create two `CronJobs`.   

Create `Inactivity Checker CronJob` for devices of type `Synfield`:
```yaml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: device-inactivity-checker-demo-synfield
spec:
  successfulJobsHistoryLimit: 1
  failedJobsHistoryLimit: 1
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          imagePullSecrets:
          - name: public-regcred
          containers:
          - name: device-inactivity-checker-demo-synfield
            image: iotngin/device-indexing-inactivity-checker:v0.1.5
            env:
              - name: ORION_URL
                value: http://orion:1026
              - name: INACTIVITY_PERIOD_SECONDS
                value: "86400"
              - name: ENTITIES_LIMIT
                value: "10" 
              - name: USE_FIWARE_SERVICE_HEADER
                value: "True"
              - name: FIWARE_SERVICE_HEADER
                value: "demo"  
              - name: FIWARE_SERVICE_PATH_HEADER
                value: "/synfield" 
          restartPolicy: OnFailure
```

Create `Inactivity Checker CronJob` for devices of type `Mobile`:
```yaml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: device-inactivity-checker-demo-mobile
spec:
  successfulJobsHistoryLimit: 1
  failedJobsHistoryLimit: 1
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          imagePullSecrets:
          - name: public-regcred
          containers:
          - name: device-inactivity-checker-demo-mobile
            image: iotngin/device-indexing-inactivity-checker:v0.1.5
            env:
              - name: ORION_URL
                value: http://orion:1026
              - name: INACTIVITY_PERIOD_SECONDS
                value: "60"
              - name: ENTITIES_LIMIT
                value: "10" 
              - name: USE_FIWARE_SERVICE_HEADER
                value: "True"
              - name: FIWARE_SERVICE_HEADER
                value: "demo"  
              - name: FIWARE_SERVICE_PATH_HEADER
                value: "/mobile" 
          restartPolicy: OnFailure
```

## Run

To start the `Inactivity Checker`, execute the following command:
```bash
kubectl apply -f config/k8s/cronjob.yml
```
